# Simple Article Editor

(이미지 업로드 기능을 포함하는) 간단한 글 편집기



## License

제작자의 허가를 받았거나 출처를 표기한 경우에 한해

(위 사항을 어기지 않으면) 자유롭게 수정/배포/사용 가능합니다.

직·간접적인 어떤 종류의 모든 보증을 부인합니다.



## Getting Started

사용법



### Prerequisites

`FileReader` API를 지원하는 브라우저

`FileReader` API를 지원하지 않으면 이미지 미리보기가 표시되지 않습니다.



### Setting

`form`의 `action`이 `server/write.php`로 되어 있습니다. 적당히 설정해 주세요!

서버에서 받는 방법은 [여기](https://gitlab.com/mjy-hobby/php/simple-article-editor)를 참조해 주세요!



## Deployment

적당히 복붙하고 스타일, `action` 바꿔주세요.



## Contributing

간단한 수정이라도 PR 환영합니다.

```C
if(cond)
{
    something();
}
```

줄바꿈 많이 쓰고, 인덴트는 Space 네 개로 해 주세요.



## Authors

- 맹주영 - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgment

- 기능 요청도 환영합니다!