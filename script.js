var imgCount = 0;
var images;
var imgCountEl;

function onselectfile(el)
{
    if(el.name)
    {
        var img = el.parentElement.childNodes[0];
        var reader = new FileReader();
        reader.onload = function(e)
        {
            img.src = e.target.result;
        }
        reader.readAsDataURL(el.files[0]);
        return;
    }
    imgCountEl.value = ++imgCount;
    var div = document.createElement("DIV");
    var img = document.createElement("IMG");
    var reader = new FileReader();
    reader.onload = function(e)
    {
        img.src = e.target.result;
    }
    reader.readAsDataURL(el.files[0]);
    div.innerHTML = "<textarea name=\"text" + imgCount +
        "\" onkeydown=\"resize(this)\" onkeyup=\"resize(this)\">";
    var newImg = el.cloneNode(false);
    newImg.name = "img" + imgCount;
    div.insertBefore(newImg, div.childNodes[0]);
    div.insertBefore(img, div.childNodes[0]);
    images.parentElement.insertBefore(div, images);
    resize(div.childNodes[2]);
    el.value = "";
}

function resize(ta)
{
    ta.style.height = "1px";
    ta.style.height = (16 + ta.scrollHeight) + "px";
}

window.addEventListener('load', function ()
{
    resize(document.forms.write.text0);
    images = document.getElementById("images");
    imgCountEl = document.getElementById("img_count");
});
